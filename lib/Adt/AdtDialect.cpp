#include "Adt/AdtDialect.h"
#include "mlir/IR/DialectImplementation.h"
#include "Adt/MemorySpecification.h"
#include <cstdint>
using namespace std;
using namespace mlir;
using namespace mlir::adt;

namespace mlir::adt {
        /// Cette classe represente le stockage interne des types sources
        struct AdtTypeStorage : public mlir::TypeStorage {
            using KeyTy = llvm::ArrayRef<mlir::Type>;
            explicit AdtTypeStorage(llvm::ArrayRef<mlir::Type> elementTypes) : elementTypes(elementTypes) {}
            llvm::ArrayRef<mlir::Type> elementTypes;
            static llvm::hash_code hashKey(const KeyTy &key) {
                return llvm::hash_value(key);
            }
            bool operator==(const KeyTy &key) const { return key == elementTypes; }

            static KeyTy getKey(llvm::ArrayRef<mlir::Type> elementTypes) {
                return KeyTy(elementTypes);
            }
            static AdtTypeStorage *construct(mlir::TypeStorageAllocator &allocator,
                                                const KeyTy &key) {
                llvm::ArrayRef<mlir::Type> elementTypes = allocator.copyInto(key);
                return new (allocator.allocate<AdtTypeStorage>())
                        AdtTypeStorage(elementTypes);
            }
        };
        /// On doit implémenter ici une instance du type type source (donc cette instance est un type aussi)
    }
///

AdtType AdtType::getType(llvm::ArrayRef<mlir::Type> elementTypes) {
    assert(!elementTypes.empty() && "expected at least 1 element type");
    mlir::MLIRContext *ctx = elementTypes.front().getContext();
    return Base::get(ctx,elementTypes);
}


AdtType AdtType::getTag(llvm::ArrayRef<Tag> elementTypes){
    assert(!elementTypes.empty() && "expected at least 1 element type");
    SmallVector<Type,1> newElementTypes;
    for (int i = 0; i < (int)elementTypes.size();i++){
        Type newElementType = (Type) (elementTypes[i].getTypeSource());
        newElementTypes.push_back(newElementType);
    }
    mlir::MLIRContext *ctx = newElementTypes.front().getContext();
    return Base::get(ctx,newElementTypes);
};

class ProductSrcType : public TypeSource{
public:
    static mlir::Type parseType(::mlir::DialectAsmParser &parser) {
        SmallVector<mlir::Type, 1> elementTypes;
        mlir::Type elementType;
        if (parser.parseCommaSeparatedList(DialectAsmParser::Delimiter::OptionalLessGreater,
                                           [&]() -> ParseResult {
                                               if (parser.parseType(elementType))
                                                   return failure();
                                               elementTypes.push_back(elementType);
                                               return success();
                                           }))
            return Type();

        return AdtType::getType(elementTypes);
    }
};

class BasicLLVMType : public TypeSource{
public: static mlir::Type parseType(::mlir::DialectAsmParser &parser){
        if (parser.parseLess())
            return Type();
        SmallVector<mlir::Type, 1> elementTypes;
        //SMLoc typeLoc = parser.getCurrentLocation();
        mlir::Type elementType;
        if (parser.parseType(elementType))
            return nullptr;
        elementTypes.push_back(elementType);

        if (parser.parseGreater())
            return Type();
        return AdtType::getType(elementTypes);
    }
};


class SumSrcType : public TypeSource{
public:
    static mlir::Type parseType(::mlir::DialectAsmParser &parser) {
        SmallVector<Tag, 1> elementTypes;
        Tag elementType;
        mlir::Type srcType;
        StringRef operand;
        if (parser.parseCommaSeparatedList(DialectAsmParser::Delimiter::OptionalLessGreater,
                                           [&]() -> ParseResult {
                                               if (failed(parser.parseKeyword(&operand)))
                                                   return failure();
                                               elementType.setConstructor(&operand);
                                               if (parser.parseColon())
                                                   return failure();
                                               if (parser.parseType(srcType))
                                                   return failure();
                                               elementType.setTypeSource(&srcType);
                                               elementTypes.push_back(elementType);
                                               return success();
                                           }))
            return Type();

        return AdtType::getTag(elementTypes);
    }
};

mlir::Type TypeSource::parseType(::mlir::DialectAsmParser &parser) {
    if (parser.parseLess())
        return Type();
    StringRef keyword;
    if (failed(parser.parseKeyword(&keyword)))
        return Type();
    if (keyword == "product")
        return ProductSrcType::parseType(parser);
    if (keyword == "sum")
        return SumSrcType::parseType(parser);
    if (keyword == "basic")
        return BasicLLVMType::parseType(parser);
    if (parser.parseGreater())
        return Type();
}



TypeMem TypeMem::parseType(::mlir::DialectAsmParser &parser){
    SMLoc typeLoc = parser.getCurrentLocation();
    TypeMem typeMem;
    StringRef keyword;
    if (parser.parseKeyword(&keyword)){
        parser.emitError(typeLoc, "error");
        return typeMem;}
    if (keyword == "i")
        return IntMemType::parseType(parser);
    if (keyword == "Word")
        return WordMemType::parseType(parser);
    if (keyword == "Ptr")
        return PtrMemType::parseType(parser);
}

TypeMem IntMemType::parseType(::mlir::DialectAsmParser &parser){
    IntMemType memType;
    int64_t size;
    SMLoc typeLoc = parser.getCurrentLocation();
    if (parser.parseInteger(size)){
        parser.emitError(typeLoc, "error ");
        return memType;
    }
    memType.setWidth(size);
    return memType;
}

TypeMem WordMemType::parseType(::mlir::DialectAsmParser &parser) {
    WordMemType memType;
    int64_t size;
    SMLoc typeLoc = parser.getCurrentLocation();
    if (parser.parseInteger(size)){
        parser.emitError(typeLoc, "error ");
        return memType;
    }
    memType.setWidth(size);
    return memType;
}

TypeMem PtrMemType::parseType(::mlir::DialectAsmParser &parser) {

}


int compareString(char* str1, const char* str2) {
    int cpt = 0;
    while (*str1 && *str2 && (*str1 == *str2)) {
        str1++;
        str2++;
        cpt++;
    }
    if (cpt == 13) {
        return 0;
    }  else {
        return 1;
    }
}

mlir::Type AdtDialect::parseType(::mlir::DialectAsmParser &parser) const{
    StringRef keyword;
    //SMLoc typeLoc = parser.getCurrentLocation();
    mlir::Type adtTypeSrc;
    TypeMem adtTypeMem;
    char * repr = "representedAs";
    if (parser.parseKeyword(&keyword))
        return Type();
    if (keyword == "type")
        adtTypeSrc = TypeSource::parseType(parser);
        StringRef keywordRepr;
        if(parser.parseKeyword(&keywordRepr))
            return Type();
        if (compareString(repr,keywordRepr.data()) != 0)
            return Type();
        else if (compareString(repr,keywordRepr.data()) == 0)
            adtTypeMem = TypeMem::parseType(parser);
    else
        return Type();
}



/// Cette fonction nous permet de print en format mlir une instance des types sources
void AdtDialect::printType(::mlir::Type type, ::mlir::DialectAsmPrinter &printer) const {
}



#define GET_OP_CLASSES
#include "Adt/AdtOps.cpp.inc"
#include "Adt/AdtOpsDialect.cpp.inc"

void AdtDialect::initialize() {
   // addOperations<
#define GET_OP_LIST
//#include "Adt/AdtOps.cpp.inc"
    //>();
    addTypes<AdtType>();
}
