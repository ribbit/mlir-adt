#include "Adt/AdtDialect.h"

using namespace mlir;

class SrcOp{

};

class SrcDerefOp : public SrcOp{

};

class SrcAccess : public SrcOp{
    int access;
};

class SrcContr : public SrcOp{
    StringRef constructor;
};

class SrcPath {
    SmallVector<SrcOp,1> sourceOpvector;
};