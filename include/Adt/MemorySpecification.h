#include "Adt/AdtDialect.h"
using namespace mlir;
using namespace mlir::adt;

class MemOp{

};

class MemDerefOp : public MemOp{

};

class MemAccess : public MemOp{
    int access;
};

class MemExtract : public MemOp{
    int Lr;
    int Br;
};

class MemPath {
    SmallVector<MemOp,1> memOpvector;
};

class Spec{
    MemPath memPath;
    TypeMem typeMem;
};

class WordMemType : public TypeMem{
private: int width;
    SmallVector<Spec> specs;
public:
    static TypeMem parseType(::mlir::DialectAsmParser &parser);
    static void setWidth(int width){
        width = width;
    }
};

class IntMemType : public TypeMem{
    int width;
public:
    static TypeMem parseType(::mlir::DialectAsmParser &parser);
    static void setWidth(int width){
        width = width;
    }
};

class PtrMemType : public TypeMem{
    int width;
    int align;
    TypeMem pointee;
    SmallVector<Spec> specs;
public:
    static TypeMem parseType(::mlir::DialectAsmParser &parser);
};