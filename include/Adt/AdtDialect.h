#ifndef ADT_DIALECT_H_
#define ADT_DIALECT_H_

#include <mlir/IR/Types.h>
#include <mlir/IR/DialectImplementation.h>
#include "mlir/IR/BuiltinOps.h"
#include "mlir/IR/BuiltinDialect.h"
#include "mlir/IR/BuiltinTypes.h"

namespace mlir::adt{
        struct AdtTypeStorage;
    }


#define GET_OP_CLASSES
#include "Adt/AdtOps.h.inc"
#include "Adt/AdtOpsDialect.h.inc"
#include "Adt/SourceSpecification.h"


/// This class defines the adt source types.


namespace mlir::adt{
    class Tag;
    class TypeSource{
    public:
        static mlir::Type parseType(::mlir::DialectAsmParser &parser);
    };
    class TypeMem{
    public:
        static TypeMem parseType(::mlir::DialectAsmParser &parser);
    };

class AdtType: public mlir::Type::TypeBase<AdtType,mlir::Type,mlir::adt::AdtTypeStorage>{
    mlir::Type typeSource;
    TypeMem typeMem;
    public:
    // Inherit some necessary constructors from 'TypeBase'.
    using Base::Base;
    // Creating an instance of a 'TypeSource' with the geven element types.
    static AdtType getTag(llvm::ArrayRef<Tag> elementTypes);
    static AdtType getType(llvm::ArrayRef<Type> elementTypes);
};

        class Tag{
        private: mlir::Type srcType;
        private: StringRef constructor;
        public: void setConstructor(StringRef* name){
                constructor = *name;
            }
        public: void setTypeSource(mlir::Type* newSrcType){
                srcType = *newSrcType;
            }
        public: mlir::Type getTypeSource() const{
                return srcType;
            }
        };
    };



#endif // ADT_DIALECT_H_